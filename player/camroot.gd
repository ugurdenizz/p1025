extends Spatial

var camrot_h = 0
var puppet_camrot_h =0

var camrot_v = 0
var cam_v_max = 75
var cam_v_min = -55
var h_sensitivity = 0.1
var v_sensitivity = 0.1
var h_acceleration = 10
var v_acceleration = 10


func _notification(what) -> void:
	if what == MainLoop.NOTIFICATION_WM_FOCUS_IN:
		Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	elif what == MainLoop.NOTIFICATION_WM_FOCUS_OUT:
		Input.set_mouse_mode(Input.MOUSE_MODE_CONFINED)
	elif what == MainLoop.NOTIFICATION_CRASH:
		Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
		
		
func _ready():
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	rset_config("puppet_h_rot", MultiplayerAPI.RPC_MODE_REMOTESYNC)
	
func _input(event):

	if event is InputEventMouseMotion:
		$Timer.start()
		camrot_h += -event.relative.x * h_sensitivity
		camrot_v += event.relative.y * v_sensitivity
		
remote func _physics_process(delta):
		$VBoxContainer/fps.text="FPS:"+str(Performance.get_monitor(Performance.TIME_FPS))
	
		$VBoxContainer/memory.text="Memory:"+str(round(Performance.get_monitor(Performance.MEMORY_STATIC)/1024/1024))
		camrot_v = clamp(camrot_v, cam_v_min, cam_v_max)
		
		var mesh_front = get_node("../Mesh").global_transform.basis.z
		var rot_speed_multiplier = 0.15 #reduce this to make the rotation radius larger
		var auto_rotate_speed =  (PI - mesh_front.angle_to($h.global_transform.basis.z)) * get_parent().velocity.length() * rot_speed_multiplier
		
		if $Timer.is_stopped():
			#FOLLOW CAMERA
			$h.rotation.y = lerp_angle($h.rotation.y, get_node("../Mesh").global_transform.basis.get_euler().y, delta * auto_rotate_speed)
			camrot_h = $h.rotation_degrees.y
		else:
			#MOUSE CAMERA
			$h.rotation_degrees.y = lerp($h.rotation_degrees.y, camrot_h, delta * h_acceleration)
		
		$h/v.rotation_degrees.x = lerp($h/v.rotation_degrees.x, camrot_v, delta * v_acceleration)
		

