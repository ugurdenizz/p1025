extends Spatial

func _ready():
	var cubicles = get_tree().get_nodes_in_group("cubicle")
	var angles=[90,-90,180,-180]
	
	for i in cubicles:
		var random_generator = RandomNumberGenerator.new()
		random_generator.randomize()
		var randAngle=random_generator.randi_range(0,3)
		var angle=angles[randAngle]
		var rot = angle
		i.rotation_degrees.y=rot

